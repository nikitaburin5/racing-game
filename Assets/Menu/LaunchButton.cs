﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LaunchButton : MonoBehaviour {

    public void LaunchLevel() {
        SceneManager.LoadScene("RaceScene");
    }
}
