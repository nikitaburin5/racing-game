﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanel : MonoBehaviour {

    private List<GameObject> toggleObjects = new List<GameObject>();
    private ToggleGroup toggleGroup;

    public Text levelInfoText;
    public SaverLoader saverLoader;
    public GameObject togglePreFab;


    private void OnEnable() {
        for (int i = 0; i < saverLoader.levels.Count; i++) {
            GameObject toggle = Instantiate(togglePreFab);
            toggle.transform.SetParent(this.gameObject.transform);
            toggle.GetComponentInChildren<Text>().text = i.ToString();
            toggleObjects.Add(toggle);
        }
    }

    private void OnDisable() {
        for (int i = 0; i < toggleObjects.Count; i++) {
            Destroy(toggleObjects[i]);
        }
        toggleObjects.Clear();
    }

    public void SetTrack(GameObject toggle) {
        int currentTrack = toggleObjects.IndexOf(toggle);
        saverLoader.currentTrack = saverLoader.levels[currentTrack];
        levelInfoText.text = saverLoader.currentTrack.name;
    }
}
