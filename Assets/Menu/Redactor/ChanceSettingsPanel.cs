﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ChanceSettingsPanel : MonoBehaviour {

    public List<GameObject> sliderObjects = new List<GameObject>();

    [SerializeField]
    public GameObject SliderPrefab;

    private void Start() {
        
        foreach (TrackPartType trackPartType in System.Enum.GetValues(typeof(TrackPartType))) {
            GameObject sliderObject = Instantiate(SliderPrefab);
            sliderObject.transform.SetParent(this.transform);
            sliderObject.GetComponentInChildren<PartChanceSlider>().identificator = trackPartType;
            sliderObjects.Add(sliderObject);
        }
    }


    public void EquilezeChanceSliders(GameObject draggedSlider) {

        float summ = 0;
        foreach (GameObject sliderObj in sliderObjects) {
            summ += sliderObj.GetComponentInChildren<Slider>().value;
        }

        foreach (GameObject sliderObj in sliderObjects) {
            if (sliderObj != draggedSlider && summ > 1 && summ != 0) {
                float a = draggedSlider.GetComponentInChildren<Slider>().value;
                sliderObj.GetComponentInChildren<Slider>().value *= 1 / summ; ;
            }
        }
    }



    
}


