﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {

    public void ShowLevel() {
        GetComponentInParent<LevelsWindow>().ShowLevelInfo(this.gameObject);
    }
}
