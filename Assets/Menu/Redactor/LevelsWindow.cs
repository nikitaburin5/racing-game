﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsWindow : MonoBehaviour {

    private Transform contentTransform;
    private List<GameObject> buttons = new List<GameObject>();
    private string defaultTrackName = "New track";


    public SaverLoader saverLoader;
    public ChanceSettingsPanel chanceSettingsPanel;
    public PitchSettingsPanel pitchSettingsPanel;
    public GameObject buttonPrefab;
    public InputField inputField;
    public GameObject difficultObject;
    public GameObject lengthObject;
    public GameObject speedObject;

    private void Start() {
        contentTransform = this.GetComponentInParent<ScrollRect>().content.transform;

        if (saverLoader.levels.Count > 0) {

            for (int i = 0; i < saverLoader.levels.Count; i++) {
                GameObject buttonObject = Instantiate(buttonPrefab);
                buttons.Add(buttonObject);
                buttonObject.transform.SetParent(contentTransform);
                buttonObject.GetComponentInChildren<Text>().text = saverLoader.levels[i].name;
            }
        }
    }

    public void ShowLevelInfo(GameObject selectedButton) {


        TrackInfo loadedTrackInfo = null;
        string trackName = selectedButton.GetComponentInChildren<Text>().text;
        for (int i = 0; i < saverLoader.levels.Count; i++) {
            if (saverLoader.levels[i].name == trackName) {
                loadedTrackInfo = saverLoader.levels[i];
                break;
            }
        }
        if (loadedTrackInfo != null) {

            inputField.text = trackName;
            difficultObject.GetComponentInChildren<Slider>().value = loadedTrackInfo.difficult;
            lengthObject.GetComponentInChildren<Slider>().value = loadedTrackInfo.segmentsCount;
            speedObject.GetComponentInChildren<Slider>().value = loadedTrackInfo.speed;

            for (int i = 0; i < chanceSettingsPanel.sliderObjects.Count; i++) {
                TrackPartType trackPartType = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<PartChanceSlider>().identificator;
                Slider slider = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                if (loadedTrackInfo.partsChances.ContainsKey(trackPartType)) {
                    slider.value = loadedTrackInfo.partsChances[trackPartType];
                }
            }

            for (int i = 0; i < pitchSettingsPanel.slidersCount; i++) {
                Slider slider = pitchSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                slider.value = loadedTrackInfo.pitchMeans[i];
            }

        }

    }

    // Make TrackInfo and put it into SaverLoader.levels and coll saving in file from SaverLoader
    public void SaveLevelInfo() {
        string newTrackName = inputField.text;

        if (newTrackName == "" || newTrackName == defaultTrackName) {
            Debug.Log("No track name");
            return;
        }

        bool isTrackNameNew = true;
        for (int i = 0; i < saverLoader.levels.Count; i++) {
            if (saverLoader.levels[i].name == newTrackName) {
                isTrackNameNew = false;
                break;
            }
        }
        // If we already have track with the same name
        if (!isTrackNameNew) {

            TrackInfo loadedTrackInfo = null;
            for (int i = 0; i < saverLoader.levels.Count; i++) {
                if (saverLoader.levels[i].name == newTrackName) {
                    loadedTrackInfo = saverLoader.levels[i];
                    break;
                }
            }

            if (loadedTrackInfo != null) {
                Dictionary<TrackPartType, float> partsChances = new Dictionary<TrackPartType, float>();
                int[] pitchMeans = new int[pitchSettingsPanel.slidersCount];

                for (int i = 0; i < chanceSettingsPanel.sliderObjects.Count; i++) {
                    TrackPartType trackPartType = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<PartChanceSlider>().identificator;
                    Slider slider = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                    partsChances.Add(trackPartType, slider.value);
                }

                for (int i = 0; i < pitchSettingsPanel.sliderObjects.Count; i++) {
                    Slider slider = pitchSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                    pitchMeans[i] = (int)slider.value;
                }
                loadedTrackInfo.speed = speedObject.GetComponentInChildren<Slider>().value;
                loadedTrackInfo.segmentsCount = (int)lengthObject.GetComponentInChildren<Slider>().value;
                loadedTrackInfo.difficult = difficultObject.GetComponentInChildren<Slider>().value;
                loadedTrackInfo.partsChances = partsChances;
                loadedTrackInfo.pitchMeans = pitchMeans;
                loadedTrackInfo.name = newTrackName;
            }
        }

        // If in inputField NEW track name
        if (isTrackNameNew) {
            TrackInfo trackInfo = new TrackInfo();
            Dictionary<TrackPartType, float> partsChances = new Dictionary<TrackPartType, float>();
            int[] pitchMeans = new int[pitchSettingsPanel.sliderObjects.Count];

            for (int i = 0; i < chanceSettingsPanel.sliderObjects.Count; i++) {
                TrackPartType trackPartType = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<PartChanceSlider>().identificator;
                Slider slider = chanceSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                partsChances.Add(trackPartType, slider.value);
            }

            for (int i = 0; i < pitchSettingsPanel.sliderObjects.Count; i++) {
                Slider slider = pitchSettingsPanel.sliderObjects[i].GetComponentInChildren<Slider>();
                pitchMeans[i] = (int)slider.value;
            }
            trackInfo.speed = speedObject.GetComponentInChildren<Slider>().value;
            trackInfo.segmentsCount = (int)lengthObject.GetComponentInChildren<Slider>().value;
            trackInfo.difficult = difficultObject.GetComponentInChildren<Slider>().value;
            trackInfo.partsChances = partsChances;
            trackInfo.pitchMeans = pitchMeans;
            trackInfo.name = newTrackName;
            saverLoader.levels.Add(trackInfo);

            GameObject buttonObject = Instantiate(buttonPrefab);
            buttonObject.transform.SetParent(contentTransform);
            buttonObject.GetComponentInChildren<Text>().text = newTrackName;
        }

        saverLoader.SaveLevels();
    }

    public void DeleteLevelInfo() {
        // If we already have track with the same name
        string deletedTrackName = inputField.text;

        
        for (int i = 0; i < saverLoader.levels.Count; i++) {
            if (saverLoader.levels[i].name == deletedTrackName) {
                saverLoader.levels.RemoveAt(i);

                for (int j = 0; j < buttons.Count; j++) {
                    if (buttons[j].GetComponentInChildren<Text>().text == deletedTrackName) {
                        GameObject deletedButton = buttons[j];
                        buttons.RemoveAt(i);
                        Destroy(deletedButton);
                        break;
                    }
                }

                break;
            }
        }

        saverLoader.SaveLevels();

    }
}
