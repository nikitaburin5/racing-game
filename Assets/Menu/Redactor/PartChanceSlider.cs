﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartChanceSlider : MonoBehaviour {

    [SerializeField]
    public Text sliderName;
    [SerializeField]
    public Text sliderMean;

    private TrackPartType m_identificator;
    public TrackPartType identificator {
        set {
            m_identificator = value;
            sliderName.text = value.ToString();
        }
        get {
            return m_identificator;
        }

    }


    public void UpdateMean() {
        sliderMean.text = Mathf.RoundToInt(this.GetComponentInChildren<Slider>().value * 100).ToString() + "%";
    }

    public void EquiliezeChanceWithMe() {
        ChanceSettingsPanel settingsPanel = GetComponentInParent<ChanceSettingsPanel>();
        settingsPanel.EquilezeChanceSliders(this.gameObject);
    }
}
