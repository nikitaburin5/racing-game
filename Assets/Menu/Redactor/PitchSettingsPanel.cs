﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PitchSettingsPanel : MonoBehaviour {

    [SerializeField]
    public GameObject vertSliderPrefab;
    public List<GameObject> sliderObjects = new List<GameObject>();


    private int m_slidersCount = 5;
    public int slidersCount {
        get {
            return m_slidersCount;
        }
    }

    private void Start() {

        for (int i = 0; i < m_slidersCount; i++) {
            GameObject sliderObject = Instantiate(vertSliderPrefab);
            sliderObject.transform.SetParent(this.transform);
            sliderObjects.Add(sliderObject);
        }
    }

    public void EquilezePitchSliders(GameObject draggedSlider) {
        /*
        int idraggedSlider = sliderObjects.IndexOf(draggedSlider);
        for (int i = idraggedSlider - 1; i >= 0; i--) {
            float delta = sliderObjects[i].GetComponentInChildren<Slider>().value - sliderObjects[i + 1].GetComponentInChildren<Slider>().value;
            if (Mathf.Abs(delta) > maxDelta) {
                sliderObjects[i].GetComponentInChildren<Slider>().value = sliderObjects[i + 1].GetComponentInChildren<Slider>().value + Mathf.Sign(delta) * maxDelta;
            }
        }

        for (int i = idraggedSlider + 1; i < slidersCount - 1; i++) {
            float delta = sliderObjects[i].GetComponentInChildren<Slider>().value - sliderObjects[i - 1].GetComponentInChildren<Slider>().value;
            if (Mathf.Abs(delta) > maxDelta) {
                sliderObjects[i].GetComponentInChildren<Slider>().value = sliderObjects[i - 1].GetComponentInChildren<Slider>().value + Mathf.Sign(delta) * maxDelta;
            }
        }*/
    }

}
