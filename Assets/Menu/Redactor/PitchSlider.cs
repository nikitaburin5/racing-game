﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PitchSlider : MonoBehaviour {

    [SerializeField]
    public Text sliderMean;

    public void UpdateMean() {
        sliderMean.text = Mathf.RoundToInt(this.GetComponentInChildren<Slider>().value).ToString();
    }

    public void EquiliezePitchWithMe() {
        PitchSettingsPanel settingsPanel = GetComponentInParent<PitchSettingsPanel>();
        settingsPanel.EquilezePitchSliders(this.gameObject);
    }
}
