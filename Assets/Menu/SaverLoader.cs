﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class TrackInfo {
    public int segmentsCount;
    public Dictionary<TrackPartType, float> partsChances;
    public int[] pitchMeans;
    public float difficult;
    public float speed;
    public string name;
    public TrackInfo() { }
    public TrackInfo(SavedTrackInfo savedTrackInfo) {
        partsChances = new Dictionary<TrackPartType, float>();
        for (int i = 0; i < savedTrackInfo.partsChances.Length; i++) {
            partsChances.Add(savedTrackInfo.trackTypes[i], savedTrackInfo.partsChances[i]);
        }
        pitchMeans = new int[savedTrackInfo.pitchMeans.Length];
        savedTrackInfo.pitchMeans.CopyTo(pitchMeans, 0);
        speed = savedTrackInfo.speed;
        segmentsCount = savedTrackInfo.segmentsCount;
        difficult = savedTrackInfo.difficult;
        name = savedTrackInfo.name;
    }
}

[Serializable]
public class SavedTrackInfo {
    public int segmentsCount;
    public TrackPartType[] trackTypes;
    public float[] partsChances;
    public int[] pitchMeans;
    public float speed;
    public float difficult;
    public string name;

    public SavedTrackInfo(TrackInfo trackInfo) {
        int chancesCount = trackInfo.partsChances.Count;
        int pitchCount = trackInfo.partsChances.Count;
        trackTypes = new TrackPartType[chancesCount];
        partsChances = new float[chancesCount];
        pitchMeans = new int[pitchCount];

        int i = 0;
        foreach (KeyValuePair<TrackPartType, float> chance in trackInfo.partsChances) {
            trackTypes[i] = chance.Key;
            partsChances[i] = chance.Value;
            i++;
        }
        trackInfo.pitchMeans.CopyTo(pitchMeans, 0);
        speed = trackInfo.speed;
        segmentsCount = trackInfo.segmentsCount;
        difficult = trackInfo.difficult;
        name = trackInfo.name;
    }
}

[Serializable]
public class SavedLevels {
    public List<SavedTrackInfo> savedlevels;

    public SavedLevels(List<TrackInfo> levels) {
        savedlevels = new List<SavedTrackInfo>();
        for (int i = 0; i < levels.Count; i++) {
            savedlevels.Add(new SavedTrackInfo(levels[i]));
        }
    }
}

[Serializable]
public class TuningProgress {
    int power;
    int control;
}

public class PlayerProgress {
    Dictionary<string, bool> careerProgress;
    TuningProgress tuningProgress;
}

public class SaverLoader : MonoBehaviour {

    public List<TrackInfo> levels = new List<TrackInfo>();
    public PlayerProgress playerProgress;

    public TrackInfo currentTrack;
    private string dataPath;


    public void SaveLevels() {

        SavedLevels savedLevels = new SavedLevels(levels);
        string jsonString = JsonUtility.ToJson(savedLevels);

        using (StreamWriter streamWriter = new StreamWriter(dataPath)) {
            streamWriter.Write(jsonString);
            streamWriter.Close();
        }
    }

    //public void SavePlayerProgress() 

    public void LoadLevels() {

        if (System.IO.File.Exists(dataPath)) {
            using (StreamReader streamReader = new StreamReader(dataPath)) {
                string jsonString = streamReader.ReadToEnd();


                SavedLevels saveLev = JsonUtility.FromJson<SavedLevels>(jsonString);

                for (int i = 0; i < saveLev.savedlevels.Count; i++) {
                    levels.Add(new TrackInfo(saveLev.savedlevels[i]));
                }
                streamReader.Close();
            }
        } else {
            TextAsset LevelsTextAsset;
            LevelsTextAsset = Resources.Load<TextAsset>("OriginalLevels");
            File.WriteAllBytes(dataPath, LevelsTextAsset.bytes);

            if (System.IO.File.Exists(dataPath)) {
                using (StreamReader streamReader = new StreamReader(dataPath)) {
                    string jsonString = streamReader.ReadToEnd();


                    SavedLevels saveLev = JsonUtility.FromJson<SavedLevels>(jsonString);

                    for (int i = 0; i < saveLev.savedlevels.Count; i++) {
                        levels.Add(new TrackInfo(saveLev.savedlevels[i]));
                    }
                    streamReader.Close();
                }
            } else {
                Debug.Log("Levels file error");
            }
        }

    }

        

    
    private void Awake() {
        dataPath = Path.Combine(Application.persistentDataPath, "LevelsParameters.txt");
        //C:/Users/Nikita/AppData/LocalLow/DefaultCompany/Mark03
        LoadLevels();
        
        //LoadPlayerProgress();

        DontDestroyOnLoad(this.gameObject);
    }

}


