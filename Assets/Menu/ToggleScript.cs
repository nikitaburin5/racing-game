﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour {

    public void SetTrack() {
        GetComponentInParent<LevelPanel>().SetTrack(this.gameObject);
    }

    private void Start() {
        Toggle toggle = GetComponent<Toggle>();
        toggle.group = GetComponentInParent<ToggleGroup>();
    }
}
