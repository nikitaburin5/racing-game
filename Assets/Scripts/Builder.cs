using System.Collections.Generic;
using UnityEngine;

public struct TrackSegment {
    public GameObject gameObject;
    public Vector3[] trajectory;

    float length;

    public Vector3 spawnPos;
    public Vector3 endPos;
    public Quaternion quatIn;
    public Quaternion quatOut;
}

public class Builder : ProcBase {

    public Vector3 QuaternionToYPR(Quaternion quat) {
        // Abbreviations for the various angular functions
        float x = quat.x;
        float y = quat.y;
        float z = quat.z;
        float w = quat.w;
        float yaw = Mathf.Rad2Deg * Mathf.Atan2(2 * y * w - 2 * x * z, 1 - 2 * y * y - 2 * z * z);
        float pitch = -Mathf.Rad2Deg * Mathf.Atan2(2 * x * w - 2 * y * z, 1 - 2 * x * x - 2 * z * z);
        float roll = Mathf.Rad2Deg * Mathf.Asin(2 * x * y + 2 * z * w);
        return new Vector3(yaw, pitch, roll);
    }

    //The width and length of each segment:
    private float m_Width = 0.5f;
    private float m_Length = 0.5f;
    private float m_Height = 0.5f;

    public int roadWidth = 8;
    public int roadHeight = 4;

    //public int m_RowSegments = 10;


    private bool m_isSegmentJustDeleted = false;
    public bool isSegmentJustDeleted { get { bool temp = m_isSegmentJustDeleted; m_isSegmentJustDeleted = false; return temp; } }

    private bool m_isSegmentJustCreated = false;
    public bool isSegmentJustCreated { get { bool temp = m_isSegmentJustCreated; m_isSegmentJustCreated = false; return temp; } }

    public GameObject TrackSegmentPrefab;
    public LineRenderer liner;
    public Generator generator;
    public CheckPointManager checkPointManager;

    public List<TrackSegment> trackSegments = new List<TrackSegment>();

    private Quaternion initQuat = Quaternion.Euler(Vector3.zero);
    private Vector3 initPos = Vector3.zero;


    private Mesh BuildMesh(Vector3[,] vertices) {
        MeshBuilder meshBuilder = new MeshBuilder();
        int rows = vertices.GetLength(0);
        int colls = vertices.GetLength(1);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colls; j++) {
                float v = 0.1f * i;
                float u = 0.1f * j;
                Vector2 uv = new Vector2(u, v);
                Vector3 offset = vertices[i, j];
                bool buildTriangles = i > 0 && j > 0;
                BuildQuadForGrid(meshBuilder, offset, uv, buildTriangles, colls);
            }
        }
        Mesh mesh = meshBuilder.CreateMesh();
        //have the mesh calculate its own normals:
        mesh.RecalculateNormals();

        return mesh;
    }

    //Should be odd because we want easily get endPos from vertices array
    private int CalcCollsOfVert(float roadWidth) {
        int temp = Mathf.RoundToInt(roadWidth / m_Width / 2);
        return temp * 2 + 1;
    }

    public struct MeshInf {
        public Vector3[,] vertices;
        public Vector3[] trajectory;
        public Vector3 endPos;
    }

    private MeshInf CalcMeshInf(Quaternion quatIn, Quaternion quatOut, float length) {



        int collsOfVerts = CalcCollsOfVert(roadWidth);
        int rowsOfVerts = Mathf.RoundToInt(length / m_Length) + 1;
        int heightsOfVerts = Mathf.RoundToInt(roadHeight / m_Height) - 1;

        Quaternion[] quat = new Quaternion[rowsOfVerts];

        for (int i = 0; i < rowsOfVerts; i++) {
            float t = 0.5f * (1.0f - Mathf.Cos((float)i / (float)(rowsOfVerts - 1) * Mathf.PI));
            t = Mathf.Clamp(t, 0, 1);
            quat[i] = Quaternion.Lerp(quatIn, quatOut, Mathf.Pow(t, 1));
        }



        int VertsInSeg1 = (collsOfVerts + 1) / 2;
        int VertsInSeg2 = heightsOfVerts;
        int VertsInSeg3 = collsOfVerts;
        int VertsInSeg4 = heightsOfVerts;
        int VertsInSeg5 = (collsOfVerts + 1) / 2;
        int VertsAcross = VertsInSeg1 + VertsInSeg2 + VertsInSeg3 + VertsInSeg4 + VertsInSeg5;

        MeshInf meshInf = new MeshInf();
        meshInf.vertices = new Vector3[rowsOfVerts, VertsAcross];
        meshInf.trajectory = new Vector3[rowsOfVerts];
        meshInf.endPos = Vector3.zero;

        Quaternion relativeQuat = Quaternion.Inverse(quatOut) * quatIn;
        float yaw = QuaternionToYPR(relativeQuat).x;
        yaw = Mathf.Clamp(yaw, -90, 90);
        //Loop through the rows:
        for (int i = 0; i < rowsOfVerts; i++) {

            Vector3 offsetAlong = Vector3.zero;
            for (int k = 1; k <= i; k++) {
                offsetAlong += quat[k - 1] * Vector3.forward * m_Length;
            }

            meshInf.trajectory[i] = offsetAlong;

            if (i == rowsOfVerts - 1 && meshInf.endPos == Vector3.zero) {
                meshInf.endPos = offsetAlong;
            }

            float halfWidth = (VertsInSeg1 - 1) * m_Width;
            float height = (VertsInSeg2 + 1) * m_Height;

            //float ccc = 0.5f*(1 - Mathf.Cos((float)i / (float)(rowsOfVerts - 1) * 2 * Mathf.PI));
            //float ccc = Mathf.Sin((float)i / (float)(rowsOfVerts - 1) * 2 * Mathf.PI);
            //float cccoefff = -yaw / 90 * 0.5f * Mathf.Pow(ccc, 1);

            //meshInf.trajectory[i] = offsetAlong + quat[i] * Vector3.right * halfWidth * 0.65f * cccoefff;



            for (int j = 0; j < VertsAcross; j++) {

                Vector3 offsetAcross = Vector3.zero;

                if (j < VertsInSeg1) {
                    float horizOffset = -m_Width * j;
                    float vertOffset = -height;
                    offsetAcross = quat[i] * (Vector3.right * horizOffset + Vector3.up * vertOffset);
                    Vector3 offset = offsetAlong + offsetAcross;
                    meshInf.vertices[i, j] = offset;
                }
                if (j < VertsInSeg1 + VertsInSeg2 && j >= VertsInSeg1) {
                    int j1 = j - VertsInSeg1;
                    float horizOffset = -halfWidth;
                    float vertOffset = -height + m_Height * (j1 + 1);
                    offsetAcross = quat[i] * (Vector3.right * horizOffset + Vector3.up * vertOffset);
                    Vector3 offset = offsetAlong + offsetAcross;
                    meshInf.vertices[i, j] = offset;
                }
                if (j < VertsInSeg1 + VertsInSeg2 + VertsInSeg3 && j >= VertsInSeg1 + VertsInSeg2) {
                    int j1 = j - VertsInSeg1 - VertsInSeg2;
                    float horizOffset = m_Width * j1 - halfWidth;
                    float vertOffset = 0;
                    offsetAcross = quat[i] * (Vector3.right * horizOffset + Vector3.up * vertOffset);
                    Vector3 offset = offsetAlong + offsetAcross;
                    meshInf.vertices[i, j] = offset;

                }
                if (j < VertsInSeg1 + VertsInSeg2 + VertsInSeg3 + VertsInSeg4 && j >= VertsInSeg1 + VertsInSeg2 + VertsInSeg3) {
                    int j1 = j - VertsInSeg1 - VertsInSeg2 - VertsInSeg3;
                    float horizOffset = halfWidth;
                    float vertOffset = -(j1 + 1) * m_Height;
                    offsetAcross = quat[i] * (Vector3.right * horizOffset + Vector3.up * vertOffset);
                    Vector3 offset = offsetAlong + offsetAcross;
                    meshInf.vertices[i, j] = offset;
                }
                if (j < VertsInSeg1 + VertsInSeg2 + VertsInSeg3 + VertsInSeg4 + VertsInSeg5 && j >= VertsInSeg1 + VertsInSeg2 + VertsInSeg3 + VertsInSeg4) {
                    int j1 = j - VertsInSeg1 - VertsInSeg2 - VertsInSeg3 - VertsInSeg4;
                    float horizOffset = halfWidth - j1 * m_Width;
                    float vertOffset = -height;
                    offsetAcross = quat[i] * (Vector3.right * horizOffset + Vector3.up * vertOffset);
                    Vector3 offset = offsetAlong + offsetAcross;
                    meshInf.vertices[i, j] = offset;
                }
            }
        }
        return meshInf;
    }

    public TrackSegment BuildSegment(Quaternion quat, float length, bool isQuaternionRelative) {




        Vector3 spawnPos = initPos;
        Quaternion quatIn = initQuat;
        Quaternion quatOut = initQuat;

        int segCount = trackSegments.Count;
        if (segCount > 0) {

            TrackSegment t = trackSegments[segCount - 1];
            spawnPos = t.endPos;
            quatIn = t.quatOut;
            if (isQuaternionRelative) {
                quatOut = quatIn * quat;
            } else {
                quatOut = quat;
            }
        };

        MeshInf meshInf = CalcMeshInf(quatIn, quatOut, length);

        GameObject segment = Instantiate(TrackSegmentPrefab, spawnPos, Quaternion.Euler(Vector3.zero));
        segment.transform.parent = this.transform;

        Mesh mesh = BuildMesh(meshInf.vertices);
        segment.GetComponent<MeshFilter>().sharedMesh = mesh;
        segment.GetComponent<MeshCollider>().sharedMesh = mesh;

        TrackSegment trackSegment = new TrackSegment();
        trackSegment.gameObject = segment;
        trackSegment.trajectory = meshInf.trajectory;
        for (int i = 0; i < trackSegment.trajectory.Length; i++) {
            trackSegment.trajectory[i] += spawnPos;
        }
        trackSegment.spawnPos = spawnPos;
        trackSegment.endPos = meshInf.endPos + spawnPos;
        trackSegment.quatIn = quatIn;
        trackSegment.quatOut = quatOut;

        trackSegments.Add(trackSegment);
        m_isSegmentJustCreated = true;
        return trackSegment;
    }

    public void PerformInstructions(List<Instruction> instructions) {
        for (int i = 0; i < instructions.Count; i++) {
            BuildSegment(instructions[i].quat, instructions[i].length, instructions[i].isRelativeQuaternion);
        }
    }

    public void DeleteLastSegment() {
        GameObject t = trackSegments[0].gameObject;
        trackSegments.RemoveAt(0);
        GameObject.Destroy(t);
        m_isSegmentJustDeleted = true;
    }


    private void Start() {

        BuildSegment(Quaternion.Euler(Vector3.zero), 50, false);
        BuildSegment(Quaternion.Euler(Vector3.zero), 50, false);

        if (generator.saverLoader != null) {
            for (int i = 0; i < generator.saverLoader.currentTrack.segmentsCount; i++) {
                List<Instruction> instructions = generator.GenerateIstructions();
                PerformInstructions(instructions);
            }
            checkPointManager.BuildCheckPoints(trackSegments);
        } else {
            for (int i = 0; i < 10; i++) {
                BuildSegment(Quaternion.FromToRotation(Vector3.forward, Vector3.left), 50, true);
                BuildSegment(Quaternion.FromToRotation(Vector3.forward, Vector3.right), 50, false);
            }
        }
        
    }

}
