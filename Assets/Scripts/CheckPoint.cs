﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        if (FindObjectOfType<CheckPointManager>() != null) {
            CheckPointManager checkPointManager = FindObjectOfType<CheckPointManager>();
            checkPointManager.currentCheckPoint = this.gameObject;
        }
    }

}
