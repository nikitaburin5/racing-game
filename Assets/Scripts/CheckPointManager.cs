﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour {

    [SerializeField]
    private int checkPointDistance = 3;
    public GameObject currentCheckPoint;

    public GameObject checkPointPrefab;
    public GameObject Player;


    public void BuildCheckPoints(List<TrackSegment> trackSegments) {
        for (int i = 0; i < trackSegments.Count; i++) {
            if (i % checkPointDistance == 0) {
                GameObject checkPoint =  Instantiate(checkPointPrefab);

                checkPoint.transform.parent = this.transform;
                checkPoint.transform.position = trackSegments[i].endPos;
                checkPoint.transform.rotation = trackSegments[i].quatOut;
            } 
        }
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.R)) {
            if (currentCheckPoint != null) {
                Player.transform.position = currentCheckPoint.transform.position + new Vector3(0, 2, 0);
                Player.transform.rotation = currentCheckPoint.transform.rotation;
                Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
                Player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            } else {
                Player.transform.position = new Vector3(0, 2, 13);
                Player.transform.rotation = Quaternion.Euler(Vector3.zero);
                Player.GetComponent<Rigidbody>().velocity = Vector3.zero;
                Player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }

        }
    }
}
