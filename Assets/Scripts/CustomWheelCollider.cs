﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CustomFrictionCurve {
    public float maxSlide = 1;
    public float frictionCoeff = 1;

    public CustomFrictionCurve(float maxSlide, float frictionCoeff) {
        this.maxSlide = maxSlide;
        this.frictionCoeff = frictionCoeff;
    }


    public float Evaluate(float slide) {
        if (Mathf.Abs(slide) < maxSlide) {
            return Mathf.Abs(slide) * frictionCoeff / maxSlide;
        } else {
            return frictionCoeff;
        }
    }
}

public class CustomWheelCollider : MonoBehaviour {

    //public GameObject wheelModel;
	[SerializeField]
    private Transform m_dummyWheel;
    private Rigidbody m_rigidbody;
    public Text textField;
    //private MyWheelFrictionCurve m_forwardFriction; //Properties of tire friction in the direction the wheel is pointing in.
    //private MyWheelFrictionCurve m_sidewaysFriction; //Properties of tire friction in the sideways direction.
    private CustomFrictionCurve m_FrictionCurve;


    private float m_inertiaMoment;
    private Vector3 m_contactSlip;
    private Vector3 m_totalForce;
    private Vector3 m_frictionForce;
    private Vector3 m_shockForce;

    private Vector3 m_center; //The center of the wheel, measured in the object's local space.
    private Vector3 m_prevPosition;
    private bool m_isGrounded; //Indicates whether the wheel currently collides with something (Read Only).

    [Header("Input")]
    [SerializeField]
    private float m_wheelMotorTorque = 0; //Motor torque on the wheel axle. Positive or negative depending on direction.
    [SerializeField]
    private float m_differentialTorque = 0; //Brake torque. Must be positive.
    [SerializeField]
    private float m_wheelBrakeTorque = 0; //Brake torque. Must be positive.

    private float m_wheelSteerAngle = 0; //Steering angle in degrees, always around the local y-axis.
    private float m_wheelAngularVelocity = 0; //Current wheel axle rotation speed, in rotations per minute (Read Only).
    private float m_wheelRotationAngle = 0;
    [Header("Wheel settings")]
    [SerializeField]
    private float m_wheelRadius = 0.5f; //The radius of the wheel, measured in local space.
    [SerializeField]
    private float m_wheelMass = 10; //The mass of the wheel. Must be larger than zero.
    [Header("Frictions settings")]
    [SerializeField]
    private float m_maxSlip = 0.1f;
    [SerializeField]
    private float m_frictionCoeff = 1;

    private RaycastHit m_raycastHit;
    [Header("Suspension settings")]
    [SerializeField]
    private float m_suspensionLength = 1; //Maximum extension distance of wheel suspension, measured in local space.
    [SerializeField]
    private float m_suspensionHardness = 10000;
    [SerializeField]
    private float m_suspensionDamp = 1000;
    private float m_suspensionCompression = 0;
    private float m_suspensionCompressionPrev = 0;

	private Utils.FilterFloat showFilter;
	private Utils.FilterFloat _meanFilter;
	private Utils.FilterVector _vectorFilter;
	//Debugging color data
	private Color GizmoColor = Color.green;

	#region Properties
	public Vector3 center {
        set {
            m_center = value;
            m_dummyWheel.localPosition = transform.localPosition + m_center;
            m_prevPosition = m_dummyWheel.localPosition;
        }
        get {
            return m_center;
        }
    }
    public float wheelRadius {
        set {
            m_wheelRadius = value;
            //m_collider.center = new Vector3(0, m_wheelRadius, 0);
        }
        get {
            return m_wheelRadius;
        }
    }
    public float suspensionLength {
        set {
            m_suspensionLength = value;
        }
        get {
            return m_suspensionLength;
        }
    }

    public float mass {
        set {
            m_wheelMass = Mathf.Max(value, 0.0001f);
        }
        get {
            return m_wheelMass;
        }
    }
    
    public float motorTorque {
        set {
            m_wheelMotorTorque = value;
        }
        get {
            return m_wheelMotorTorque;
        }
    }
    public float differentialTorque {
        set {
            m_differentialTorque = value;
        }
        get {
            return m_differentialTorque;
        }
    }
    public float brakeTorque {
        set {
            m_wheelBrakeTorque = value;
        }
        get {
            return m_wheelBrakeTorque;
        }
    }
    public float steerAngle {
        set {
            m_wheelSteerAngle = value;
        }
        get {
            return m_wheelSteerAngle;
        }
    }
    public bool isGrounded {
        get {
            return m_isGrounded;
        }
    }
    public float rpm {
        get {
            return m_wheelAngularVelocity;
        }
    }
    public float inertiaMoment {
        get {
            return m_inertiaMoment;
        }
    }
	#endregion
	// Use this for initialization
	public void Awake() {
		//if (m_dummyWheel == null)
			//m_dummyWheel = Instantiate(wheelModel).transform;

        m_dummyWheel.SetParent(this.transform);
        if (transform.localPosition.x < 0) {
            m_dummyWheel.transform.localScale = new Vector3(-1, 1, 1);
        }
        m_FrictionCurve = new CustomFrictionCurve(m_maxSlip, m_frictionCoeff);
        m_inertiaMoment = m_wheelMass * m_wheelRadius * m_wheelRadius * 0.5f;
    }

    public void Start() {
		//Find the rigidbody associated with the wheel
		showFilter = new Utils.FilterFloat(0, 1);
		_meanFilter = new Utils.FilterFloat(0, Time.deltaTime * 5);
        Transform parentTransform = transform.parent;
        if (GetComponentInParent<Rigidbody>() != null) {
            m_rigidbody = GetComponentInParent<Rigidbody>();
        }

        if (m_rigidbody == null) {
            Debug.LogError("WheelColliderSource: Unable to find associated Rigidbody.");
        }
    }

    // Called once per physics update
    public void FixedUpdate() {

        UpdateSuspension();

        UpdateWheel();

        if (m_isGrounded) {
            CalculateSlips();

            CalculateForcesFromSlips();
			var temp = transform.TransformPoint(Vector3.down * (m_wheelRadius + m_suspensionLength - m_suspensionCompression));

			m_rigidbody.AddForceAtPosition(m_totalForce, temp);
			if (textField != null)
			{
				textField.text = temp.ToString() + " " + transform.position;
			}
		}
    }

    public void OnDrawGizmosSelected() {
        if (m_dummyWheel != null) {
            Gizmos.color = GizmoColor;
            //Draw the suspension
            Gizmos.DrawLine(transform.position, m_dummyWheel.position);

            //Draw the wheel
            Vector3 point1;
            Vector3 point0 = m_dummyWheel.transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin(0), Mathf.Cos(0)));

            int pointsCount = 20;
            for (int i = 1; i <= pointsCount; ++i) {
                point1 = m_dummyWheel.transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin((float)i / pointsCount * Mathf.PI * 2.0f), Mathf.Cos((float)i / pointsCount * Mathf.PI * 2.0f)));
                Gizmos.DrawLine(point0, point1);
                point0 = point1;

            }

        }

        if (m_dummyWheel == null) {
            Gizmos.color = Color.green;
            //Draw the suspension
            //Draw the wheel
            Vector3 point1;
            Vector3 point0 = transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin(0), Mathf.Cos(0)) + Vector3.down * m_suspensionLength);

            int pointsCount = 20;
            for (int i = 1; i <= pointsCount; ++i) {
                point1 = transform.TransformPoint(m_wheelRadius * new Vector3(0, Mathf.Sin((float)i / pointsCount * Mathf.PI * 2.0f), Mathf.Cos((float)i / pointsCount * Mathf.PI * 2.0f)) + Vector3.down * m_suspensionLength);
                Gizmos.DrawLine(point0, point1);
                point0 = point1;

            }
        }

    }
   

    private void UpdateSuspension() {
        //Raycast down along the suspension to find out how far the ground is to the wheel
        bool result = Physics.Raycast(new Ray(this.transform.position, -this.transform.up), out m_raycastHit, m_wheelRadius + m_suspensionLength);

		if (result) //The wheel is in contact with the ground
        {
            if (!m_isGrounded) //If not previously grounded, set the prevPosition value to the wheel's current position.
            {
                m_prevPosition = m_dummyWheel.position;
            }
            GizmoColor = Color.green;
            m_isGrounded = true;

            //Store the previous suspension compression for the damping calculation
            m_suspensionCompressionPrev = m_suspensionCompression;

            //Update the suspension compression
            m_suspensionCompression = m_suspensionLength + m_wheelRadius - (m_raycastHit.point - this.transform.position).magnitude;
            if (m_suspensionCompression > m_suspensionLength) {
                GizmoColor = Color.red;
            }
        } else //The wheel is in the air
          {
            m_suspensionCompression = 0;
            GizmoColor = Color.blue;
            m_isGrounded = false;
        }

    }

    private void UpdateWheel() {

        transform.localEulerAngles = new Vector3(0, m_wheelSteerAngle, 0);
        //Calculate the wheel's rotation given it's angular velocity
        m_wheelRotationAngle += m_wheelAngularVelocity * Mathf.Rad2Deg * Time.deltaTime;

        //Set the rotation and steer angle of the wheel model
        m_dummyWheel.localEulerAngles = new Vector3(m_wheelRotationAngle, 0, 0);

        //Set the wheel's position given the current suspension compression
        m_dummyWheel.transform.localPosition = Vector3.down * (m_suspensionLength - m_suspensionCompression);



        float frictionTorque = Vector3.Dot(m_frictionForce, transform.TransformVector(Vector3.forward)) * m_wheelRadius;

        float m_summaryTorque = frictionTorque + motorTorque + differentialTorque;

        
        if (brakeTorque != 0) {
            if (Mathf.Abs(m_wheelAngularVelocity) > brakeTorque / m_inertiaMoment * 2) {
                m_summaryTorque -= Mathf.Sign(m_wheelAngularVelocity) * brakeTorque;
            } else {
                m_wheelAngularVelocity = 0;
                m_summaryTorque = 0;
            }
        }
        
        //Apply motor torque
        var newAngularVelocity = m_wheelAngularVelocity + m_summaryTorque / m_inertiaMoment * Time.deltaTime;
		m_wheelAngularVelocity = _meanFilter.Filtrate(newAngularVelocity, Time.deltaTime);

	}

    private void CalculateSlips() {
        //Calculate the wheel's linear velocity
        Vector3 velocity = (transform.position - m_prevPosition) / Time.deltaTime;
        velocity = Vector3.ProjectOnPlane(velocity, transform.TransformVector(Vector3.down));
        m_prevPosition = transform.position;

        Vector3 contactVelocity = m_wheelAngularVelocity * m_wheelRadius * transform.TransformVector(Vector3.back);
        m_contactSlip = velocity + contactVelocity;
    }

    private void CalculateForcesFromSlips() {
        //Spring force
        Vector3 m_springForce = transform.up * m_suspensionCompression * m_suspensionHardness;

        //Spring damping force
        Vector3 m_dampingForce = transform.up * (m_suspensionCompression - m_suspensionCompressionPrev) / Time.deltaTime * m_suspensionDamp;

        m_shockForce = m_springForce + m_dampingForce;

        float m_falseFrictionCoeff = m_FrictionCurve.Evaluate(m_contactSlip.magnitude);
		m_frictionForce = m_contactSlip.normalized * m_falseFrictionCoeff * m_shockForce.magnitude;

        m_totalForce = m_shockForce - m_frictionForce;

    }
}