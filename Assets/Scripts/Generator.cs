﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum TrackPartType {
    Turn,
    SpeedTurn,
    MegaTurn,
    DangerousTurn,
    Straight,
    Loop,
    LoopInTurn
}

public struct Instruction {

    public Quaternion quat;
    public float length;
    public bool isRelativeQuaternion;

    public Instruction(Quaternion quat, float length, bool isRelativeQuaternion) {
        this.isRelativeQuaternion = isRelativeQuaternion;
        this.quat = quat;
        this.length = length;
    }
}

public class SegmentParameters {
    public TrackPartType trackPartType;
    public float speed;
    public float randomCoeff;
    public float pitch;
}

class ParametersNarrator {

    private int segmentNumber = 0;
    List<TrackPartType> listTPT = new List<TrackPartType>();
    List<float> listTPTCtable = new List<float>();
    TrackInfo trackInfo;

    private float PitchFunction(int segmentNumber, int segmentsCount, int[] pitchMeans) {
        int pitchMeansCount = pitchMeans.Length;
        int pitchBeacon = 1;
        
        while ((float)pitchBeacon / (pitchMeansCount - 1) < (float)segmentNumber / (segmentsCount - 1)) {
            pitchBeacon++;
        }
        pitchBeacon = Mathf.Clamp(pitchBeacon, 0, pitchMeansCount - 1);
        float lerpCoeff = ((float)segmentNumber / (segmentsCount - 1) - (float)(pitchBeacon - 1) / (pitchMeansCount - 1)) /
            ((float)(pitchBeacon) / (pitchMeansCount - 1) - (float)(pitchBeacon - 1) / (pitchMeansCount - 1));

        float pitch = Mathf.Lerp(pitchMeans[pitchBeacon - 1], pitchMeans[pitchBeacon], lerpCoeff);
        return pitch;
    }

    private float RandomCoeff(float difficult) {
        difficult = Mathf.Clamp(difficult, 0.0f, 0.5f);        
        return Random.Range(1 - difficult, 1 + difficult);
    }

    private TrackPartType PartTypeRandomCalc() {
        float value = Random.value;
        int i = 0;
        while (value > listTPTCtable[i]) {
            i++;
            if (i > listTPTCtable.Count - 1) {
                break;
            }
        }
        i = Mathf.Clamp(i, 0, listTPTCtable.Count - 1);
        return listTPT[i];
    }

    public ParametersNarrator(TrackInfo trackInfo) {
        this.trackInfo = trackInfo;
        foreach (KeyValuePair<TrackPartType, float> partChance in trackInfo.partsChances) {
            listTPT.Add(partChance.Key);
            listTPTCtable.Add(partChance.Value);
        }
        for (int i = 1; i < listTPTCtable.Count; i++) {
            listTPTCtable[i] += listTPTCtable[i - 1];
        }
    }

    public SegmentParameters CalcSegmentParameters() {

        SegmentParameters segmentParameters = new SegmentParameters();
        segmentParameters.trackPartType = PartTypeRandomCalc();
        segmentParameters.pitch = PitchFunction(segmentNumber, trackInfo.segmentsCount, trackInfo.pitchMeans);
        segmentParameters.randomCoeff = RandomCoeff(trackInfo.difficult);
        segmentParameters.speed = Mathf.Clamp(trackInfo.speed, 1, 3);
        segmentNumber++;
        return segmentParameters;
    }
}

public class Generator : MonoBehaviour {
    // Frequency of different objects

    public Builder builder;

    ParametersNarrator narrator;
    private PartsRandomCalculator PRC;
    public List<Instruction> instructions = new List<Instruction>();
    [HideInInspector]
    public SaverLoader saverLoader = null;

    public List<Instruction> GenerateIstructions() {
        Quaternion quatOut = builder.trackSegments[builder.trackSegments.Count - 1].quatOut;
        SegmentParameters segmentParameters = narrator.CalcSegmentParameters();
        List<Instruction> newInstructions = PRC.CalcNewInstructions(quatOut, segmentParameters);
        return newInstructions;
    }


    private void Awake() {
        if (FindObjectOfType<SaverLoader>() != null) {
            saverLoader = FindObjectOfType<SaverLoader>();
            PRC = GetComponent<PartsRandomCalculator>();

            narrator = new ParametersNarrator(saverLoader.currentTrack);

        } 
    }
    private void OnDestroy() {
        if (saverLoader != null) {
            Destroy(saverLoader.gameObject);
        }
    }
}

