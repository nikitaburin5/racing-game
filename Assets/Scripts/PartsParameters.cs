﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
public enum TrackPartType {
    Turn,
    LongTurn,
    SpeedTurn,
    DangerousTurn,
    Straight,
    Jump,
    Loop
}
*/

public class PartsParameters : MonoBehaviour {

    [SerializeField]
    public float LoopSegmentLength = 1;
    [SerializeField]
    public float SpeedTurnEntryLength = 1;
    [SerializeField]
    public float SpeedTurnMainLength = 2;
    [SerializeField]
    public float MegaTurnRoll = 3;
    [SerializeField]
    public float DangerousTurnRoll = 0.5f;
    [SerializeField]
    public float MegaTurnEntryLength = 1.5f;
    [SerializeField]
    public float MegaTurnMainLength = 3;
}
