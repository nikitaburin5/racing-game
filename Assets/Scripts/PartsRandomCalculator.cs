﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PartsRandomCalculator : MonoBehaviour {


    public Quaternion YPRtoQuaternion(float _yaw, float _pitch, float _roll) {
        // Abbreviations for the various angular functions
        float yaw = _yaw * Mathf.Deg2Rad;
        float pitch = -_pitch * Mathf.Deg2Rad;
        float roll = _roll * Mathf.Deg2Rad;

        float cy = Mathf.Cos(yaw * 0.5f);
        float sy = Mathf.Sin(yaw * 0.5f);
        float cp = Mathf.Cos(pitch * 0.5f);
        float sp = Mathf.Sin(pitch * 0.5f);
        float cr = Mathf.Cos(roll * 0.5f);
        float sr = Mathf.Sin(roll * 0.5f);

        Quaternion q;
        q.w = cy * cp * cr + sy * sp * sr;
        q.z = cy * cp * sr - sy * sp * cr;
        q.x = sy * cp * sr + cy * sp * cr;
        q.y = sy * cp * cr - cy * sp * sr;
        return q;
    }

    public Vector3 QuaternionToYPR(Quaternion quat) {
        // Abbreviations for the various angular functions
        float x = quat.x;
        float y = quat.y;
        float z = quat.z;
        float w = quat.w;
        float yaw = Mathf.Rad2Deg * Mathf.Atan2(2 * y * w - 2 * x * z, 1 - 2 * y * y - 2 * z * z);
        float pitch = - Mathf.Rad2Deg * Mathf.Atan2(2 * x * w - 2 * y * z, 1 - 2 * x * x - 2 * z * z);
        float roll = Mathf.Rad2Deg * Mathf.Asin(2 * x * y + 2 * z * w);
        return new Vector3(yaw, pitch, roll);
    }

    public float RandomSign() {
        return (Random.value < 0.5f ? 1.0f : -1.0f);
    }

    private PartsParameters parameters;

    public float minLength = 40;
    public float difficultCoeff = 0.25f;


    public float UsualTurnYaw = - 90;
    public float UsualTurnRoll = 15;

    private Quaternion initQuat = Quaternion.Euler(Vector3.zero);

    public List<Instruction> CalcNewInstructions(Quaternion quat0, SegmentParameters segmentParameters) {
        float speedCoeff = segmentParameters.speed;
        float randomCoeff = segmentParameters.randomCoeff;
        float leftOrRight = RandomSign();
        float pitch = segmentParameters.pitch;
        //Quaternion quat0 = builder.trackSegments[builder.trackSegments.Count - 1].quatOut;
        List<Instruction> newInstructions = new List<Instruction>();
        switch (segmentParameters.trackPartType) {
            case TrackPartType.Turn: {
                    float length = minLength * speedCoeff * randomCoeff;

                    float yaw = UsualTurnYaw * randomCoeff * leftOrRight;
                    Vector3 YPR = QuaternionToYPR(quat0);
                    Quaternion quat1 = YPRtoQuaternion(yaw + YPR.x, pitch, 0);

                    Instruction instruction1 = new Instruction(quat1, length, false);
                    newInstructions.Add(instruction1);
                    break;
                }
            case TrackPartType.SpeedTurn: {
                    float entryLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnEntryLength;
                    float mainLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnMainLength;

                    float roll = UsualTurnRoll * leftOrRight;
                    float yaw = UsualTurnYaw * randomCoeff * leftOrRight;
                    Vector3 YPR = QuaternionToYPR(quat0);

                    Quaternion quat1 = YPRtoQuaternion(+YPR.x, pitch, roll);
                    Quaternion quat2 = YPRtoQuaternion( yaw + YPR.x, pitch, roll);
                    Quaternion quat3 = YPRtoQuaternion( yaw + YPR.x, pitch, 0);

                    //Quaternion relativeQuat12 = Quaternion.Inverse(quat1) * quat2;
                    //Quaternion relativeQuat23 = Quaternion.Inverse(quat2) * quat3;
                    Instruction instruction1 = new Instruction(quat1, entryLength, false);
                    Instruction instruction2 = new Instruction(quat2, mainLength, false);
                    Instruction instruction3 = new Instruction(quat3, entryLength, false);
                    newInstructions.Add(instruction1);
                    newInstructions.Add(instruction2);
                    newInstructions.Add(instruction3);
                    break;
                }
            case TrackPartType.DangerousTurn: {
                    float entryLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnEntryLength;
                    float mainLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnMainLength;

                    float roll = -UsualTurnRoll * leftOrRight * parameters.DangerousTurnRoll;
                    float yaw = UsualTurnYaw * randomCoeff * leftOrRight;
                    Vector3 YPR = QuaternionToYPR(quat0);

                    Quaternion quat1 = YPRtoQuaternion(+YPR.x, pitch, roll);
                    Quaternion quat2 = YPRtoQuaternion(yaw + YPR.x, pitch, roll);
                    Quaternion quat3 = YPRtoQuaternion(yaw + YPR.x, pitch, 0);

                    //Quaternion relativeQuat12 = Quaternion.Inverse(quat1) * quat2;
                    //Quaternion relativeQuat23 = Quaternion.Inverse(quat2) * quat3;
                    Instruction instruction1 = new Instruction(quat1, entryLength, false);
                    Instruction instruction2 = new Instruction(quat2, mainLength, false);
                    Instruction instruction3 = new Instruction(quat3, entryLength, false);
                    newInstructions.Add(instruction1);
                    newInstructions.Add(instruction2);
                    newInstructions.Add(instruction3);
                    break;
                }
            case TrackPartType.MegaTurn: {
                    float entryLength = minLength * speedCoeff * randomCoeff * parameters.MegaTurnEntryLength;
                    float mainLength = minLength * speedCoeff * randomCoeff * parameters.MegaTurnMainLength;

                    float roll = UsualTurnRoll * parameters.MegaTurnRoll * randomCoeff * leftOrRight;
                    float yaw = UsualTurnYaw * randomCoeff * leftOrRight;
                    Vector3 YPR = QuaternionToYPR(quat0);

                    Quaternion quat1 = YPRtoQuaternion(+YPR.x, pitch, roll);
                    Quaternion quat2 = YPRtoQuaternion(yaw + YPR.x, pitch, roll);
                    Quaternion quat3 = YPRtoQuaternion(yaw + YPR.x, pitch, 0);

                    //Quaternion relativeQuat12 = Quaternion.Inverse(quat1) * quat2;
                    //Quaternion relativeQuat23 = Quaternion.Inverse(quat2) * quat3;
                    Instruction instruction1 = new Instruction(quat1, entryLength, false);
                    Instruction instruction2 = new Instruction(quat2, mainLength, false);
                    Instruction instruction3 = new Instruction(quat3, entryLength, false);
                    newInstructions.Add(instruction1);
                    newInstructions.Add(instruction2);
                    newInstructions.Add(instruction3);
                    break;
                }            
            case TrackPartType.Straight: {
                    float length = minLength * speedCoeff * randomCoeff;

                    Vector3 YPR = QuaternionToYPR(quat0);
                    Quaternion quat1 = YPRtoQuaternion(YPR.x, pitch, 0);

                    Instruction instruction1 = new Instruction(quat1, length, true);
                    newInstructions.Add(instruction1);
                    break;
                }
            case TrackPartType.Loop: {
                    float length = minLength * randomCoeff * parameters.LoopSegmentLength;
                    float topOffset = 0.3f;
                    Quaternion quat11 = Quaternion.FromToRotation(Vector3.forward, Vector3.up);
                    Quaternion quat12 = Quaternion.FromToRotation(Vector3.right, Vector3.right + Vector3.down * topOffset);
                    Quaternion quat = quat11 * quat12;
                    
                    Instruction instruction1 = new Instruction(quat, length, true);
                    Instruction instruction2 = new Instruction(quat, length, true);
                    Instruction instruction3 = new Instruction(quat, length, true);
                    Instruction instruction4 = new Instruction(quat, length, true);
                    newInstructions.Add(instruction1);
                    newInstructions.Add(instruction2);
                    newInstructions.Add(instruction3);
                    newInstructions.Add(instruction4);
                    break;
                }
            case TrackPartType.LoopInTurn: {
                    float loopLength = minLength * randomCoeff * parameters.LoopSegmentLength;
                    float topOffset = 0.3f * leftOrRight;
                    Quaternion quat11 = Quaternion.FromToRotation(Vector3.forward, Vector3.up);
                    Quaternion quat12 = Quaternion.FromToRotation(Vector3.right, Vector3.right + Vector3.down * topOffset);
                    Quaternion quat = quat11 * quat12;

                    Instruction loopInstruction1 = new Instruction(quat, loopLength, true);
                    Instruction loopInstruction2 = new Instruction(quat, loopLength, true);
                    Instruction loopInstruction3 = new Instruction(quat, loopLength, true);
                    Instruction loopInstruction4 = new Instruction(quat, loopLength, true);

                    float entryLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnEntryLength;
                    float mainLength = minLength * speedCoeff * randomCoeff * parameters.SpeedTurnMainLength;

                    float roll = UsualTurnRoll * randomCoeff * leftOrRight * 2;
                    float yaw = UsualTurnYaw * randomCoeff * leftOrRight;
                    Vector3 YPR = QuaternionToYPR(quat0);

                    Quaternion quat1 = YPRtoQuaternion(+YPR.x, pitch, roll);
                    Quaternion quat2 = YPRtoQuaternion(yaw / 2 + YPR.x, pitch, roll);
                    Quaternion quat3 = YPRtoQuaternion(yaw + YPR.x, pitch, roll);
                    Quaternion quat4 = YPRtoQuaternion(yaw + YPR.x, pitch, 0);

                    Instruction instruction1 = new Instruction(quat1, entryLength, false);
                    Instruction instruction2 = new Instruction(quat2, mainLength, false);
                    Instruction instruction3 = new Instruction(quat3, mainLength, false);
                    Instruction instruction4 = new Instruction(quat4, entryLength, false);

                    newInstructions.Add(instruction1);
                    newInstructions.Add(instruction2);
                    newInstructions.Add(loopInstruction1);
                    newInstructions.Add(loopInstruction2);
                    newInstructions.Add(loopInstruction3);
                    newInstructions.Add(loopInstruction4);
                    newInstructions.Add(instruction3);
                    newInstructions.Add(instruction4);
                    break;
                }
        }
        return newInstructions;
    }

    private void Awake() {
        parameters = GetComponent<PartsParameters>();
    }
}
