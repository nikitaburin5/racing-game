﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
	public abstract class Filter<T>
	{
		private T _filteredMean;
		private float _filterTime;
		public Filter(T startMean, float filterTime)
		{
			_filteredMean = startMean;
			_filterTime = filterTime;
		}

		public float FilterTime
		{
			get
			{
				return _filterTime;
			}
			set
			{
				_filterTime = value;
			}
		}

		public T FilteredMean
		{
			get
			{
				return _filteredMean;
			}
		}

		protected abstract T MoveTowards(T current, T target, float ratio);

		public T Filtrate(T mean, float timeSinceLastUpdate = 0.02f)
		{
			_filteredMean = MoveTowards(_filteredMean, mean, timeSinceLastUpdate / _filterTime);
			return _filteredMean;
		}
	}

	public class FilterFloat : Filter<float>
	{
		public FilterFloat(float startMean, float filterTime) : base(startMean, filterTime)
		{
		}

		protected override float MoveTowards(float current, float target, float ratio)
		{
			float delta = target - current;
			return current + delta * ratio;
		}
	}

	public class FilterVector : Filter<Vector3>
	{
		public FilterVector(Vector3 startMean, float filterTime) : base(startMean, filterTime)
		{
		}

		protected override Vector3 MoveTowards(Vector3 current, Vector3 target, float ratio)
		{
			Vector3 delta = target - current;
			return current + delta * ratio;
		}
	}

}