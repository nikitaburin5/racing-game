﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public enum DriveType
{
	RearWheelDrive,
	FrontWheelDrive,
	AllWheelDrive
}

[RequireComponent(typeof(Rigidbody))]
public class WheelDrive : MonoBehaviour {

    private float smoothedValue;
    private float SmoothValue(float target) {
        target = Input.GetAxisRaw("Horizontal");
        if ((target > 0 && smoothedValue < 0) || (target < 0 && smoothedValue > 0) || target == 0) {
            smoothedValue = 0;
        }
        smoothedValue = Mathf.MoveTowards(smoothedValue, target, sensitivity * Time.deltaTime);
        return smoothedValue;
    }

    [Tooltip("Maximum steering angle of the wheels")]
    public float maxAngle = 30f;
    [Tooltip("Maximum torque applied to the driving wheels")]
    public float maxTorque = 300f;
    [Tooltip("Maximum brake torque applied to the driving wheels")]
    public float brakeTorque = 30000f;
    [Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
    public GameObject wheelShape;

    [Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
    public float criticalSpeed = 5f;
    [Tooltip("Simulation sub-steps when the speed is above critical.")]
    public int stepsBelow = 5;
    [Tooltip("Simulation sub-steps when the speed is below critical.")]
    public int stepsAbove = 1;

    [Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
    public DriveType driveType;

    [SerializeField]
    private float sensitivity = 2.0f;
	[SerializeField]
	bool _handBrake = false;

    //public AI AutoPilot;
    public Text textField;
    Rigidbody rb;
    private CustomWheelCollider[] m_Wheels;

    // Find all the WheelColliders down in the hierarchy.
	void Start()
	{
		m_Wheels = GetComponentsInChildren<CustomWheelCollider>();

        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = Vector3.down * 0.08f;
    }

	// This is a really simple approach to updating wheels.
	// We simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero.
	// This helps us to figure our which wheels are front ones and which are rear.
	void Update()
	{

        //m_Wheels[0].ConfigureVehicleSubsteps(criticalSpeed, stepsBelow, stepsAbove);

        /*float autoPilotAngle = 0;
        bool autoPilotOn = Input.GetKey(KeyCode.Space);
        if (AutoPilot != null) {
            if (autoPilotOn) {
                autoPilotAngle = AutoPilot.CalcSteerAngle(transform, 2);
            }
        }*/

        //float velocity = rb.velocity.magnitude;
        //float angleCoeff = 10 / Mathf.Pow((velocity + 1), 0.3f);
        //gleCoeff = Mathf.Clamp(angleCoeff, 0.01f, 1);
        float maxAngleCalculated = 2 * 3 * Physics.gravity.magnitude * 1.5f / Mathf.Max(rb.velocity.sqrMagnitude, 10) * Mathf.Rad2Deg;
        maxAngleCalculated = Mathf.Clamp(maxAngleCalculated, -maxAngle, maxAngle);

        float helpAngle = Vector3.Dot(rb.velocity.normalized, transform.TransformVector(Vector3.right)) * Mathf.Rad2Deg;
        //helpAngle = Mathf.Clamp(helpAngle, -maxAngle, +maxAngle);
        float angle = SmoothValue(Input.GetAxisRaw("Horizontal")) * maxAngle;
        //float angle = maxAngle * Input.GetAxis("Horizontal") + autoPilotAngle;
        float currentTorque = maxTorque * (1 - rb.velocity.magnitude / 300);
        float torque = Input.GetKey(KeyCode.UpArrow) ? currentTorque : 0;
        float brake = Input.GetKey(KeyCode.DownArrow) ? brakeTorque : 0;
        float handBrake = Input.GetKey(KeyCode.Space) || _handBrake ? brakeTorque : 0;

		foreach (CustomWheelCollider wheel in m_Wheels)
		{
            // A simple car where front wheels steer while rear ones drive.
            wheel.brakeTorque = brake;
            if (wheel.transform.localPosition.z > 0)
				wheel.steerAngle = angle;

			if (wheel.transform.localPosition.z < 0)
			{
				wheel.brakeTorque += handBrake;
			}

			if (wheel.transform.localPosition.z < 0 && driveType != DriveType.FrontWheelDrive)
			{
				wheel.motorTorque = torque;
			}

			if (wheel.transform.localPosition.z >= 0 && driveType != DriveType.RearWheelDrive)
			{
				wheel.motorTorque = torque;
			}

            /*// Update visual wheels if any.
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// Assume that the only child of the wheelcollider is the wheel shape.
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}*/
        }
	}
}
