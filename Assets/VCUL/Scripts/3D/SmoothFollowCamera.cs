﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowCamera : MonoBehaviour
{
    [Header("Camera")]
    [Tooltip("Reference to the target GameObject.")]
    public Transform target;

    public Vector3 usualViewOffset = new Vector3(0, 2, -4);
    public Vector3 backViewOffset = new Vector3(0, 2, 4);
    public Vector3 leftViewOffset = new Vector3(-4, 2, 0);
    public Vector3 rightViewOffset = new Vector3(4, 2, 0);
    public Vector3 topViewOffset = new Vector3(0, 4, 0);

    public Rigidbody targetRigidbody;

    public float smoothTime = 0.05f;

    private Vector3 offset = Vector3.zero;

    // LateUpdate is called every frame, if the Behaviour is enabled 
    private void FixedUpdate()
    {
        
        if (Input.GetKey(KeyCode.A)) {
            offset = leftViewOffset;
        }
        if (Input.GetKey(KeyCode.D)) {
            offset = rightViewOffset;
        }
        if (Input.GetKey(KeyCode.S)) {
            offset = backViewOffset;
        }
        if (Input.GetKey(KeyCode.W)) {
            offset = topViewOffset;
        }

        Vector3 currentVelocity = targetRigidbody.velocity;
        transform.position = Vector3.SmoothDamp(transform.position, target.TransformPoint(offset), ref currentVelocity, smoothTime);
        transform.LookAt(target.position);

        offset = usualViewOffset;
    }
}
